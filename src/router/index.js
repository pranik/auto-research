import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/view/login/login'
import homepage from '@/view/homepage/homepage'

Vue.use(Router)

export default new Router ({
	mode: 'history',
	routes: [
		{
			path: '/login',
			name: 'login',
			component: Login
		},
		{
			path: '/',
			name: 'homepage',
			component: homepage
		},
	]
})